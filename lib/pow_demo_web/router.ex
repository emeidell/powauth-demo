defmodule PowDemoWeb.Router do
  use PowDemoWeb, :router
  use Pow.Phoenix.Router
  use Pow.Extension.Phoenix.Router,
    extensions: [PowResetPassword, PowEmailConfirmation]

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :protected do
    plug Pow.Plug.RequireAuthenticated,
      error_handler: Pow.Phoenix.PlugErrorHandler
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  if Mix.env == :dev do
    forward "/sent_emails", Bamboo.SentEmailViewerPlug
  end


  scope "/" do
    pipe_through :browser

    get "/", PowDemoWeb.PageController, :index

    pow_routes()
    pow_extension_routes()
  end

  scope "/", PowDemoWeb do
    pipe_through [:browser, :protected]

    get "/secure", SecureController, :index

    # Add your protected routes here
  end

  # Other scopes may use custom stacks.
  # scope "/api", PowDemoWeb do
  #   pipe_through :api
  # end
end
