defmodule PowDemoWeb.PowResetPassword.MailerView do
  use PowDemoWeb, :mailer_view

  def subject(:reset_password, _assigns), do: "Reset password link"
end
