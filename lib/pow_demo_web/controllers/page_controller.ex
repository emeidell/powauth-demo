defmodule PowDemoWeb.PageController do
  use PowDemoWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
