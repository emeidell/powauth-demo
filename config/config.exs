# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :pow_demo,
  ecto_repos: [PowDemo.Repo]

# Configures the endpoint
config :pow_demo, PowDemoWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "LvoyhLv8PgjkyqC3vXow3xtV3QPOCflN44CZopAyoJNw/7q/pZz2sV6ksFnobrN8",
  render_errors: [view: PowDemoWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: PowDemo.PubSub, adapter: Phoenix.PubSub.PG2],
  live_view: [signing_salt: "29gYI/Xh"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Configure Pow
config :pow_demo, :pow,
  user: PowDemo.Users.User,
  repo: PowDemo.Repo,
  extensions: [PowEmailConfirmation, PowResetPassword],
  controller_callbacks: Pow.Extension.Phoenix.ControllerCallbacks,
  mailer_backend: PowDemoWeb.PowMailer,
  web_module: PowDemoWeb

# Mailer config
config :pow_demo, PowDemoWeb.PowMailer,
  adapter: Bamboo.LocalAdapter,
  open_email_in_browser_url: "http://localhost:4000/sent_emails"

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
